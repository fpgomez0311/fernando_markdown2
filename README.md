**Práctica 2**

1. Repositorio y clonación 
__

![1](Imagenes/1.PNG)
![2](Imagenes/2.PNG)

2. Add, commit, push

![3](Imagenes/3.PNG)

3. Carpeta privada y .gitignore

![4](Imagenes/4.PNG)
![5](Imagenes/5.PNG)

4. Tag

![6](Imagenes/6.PNG)

5.Tabla


| Nombre | Repositorio |
| ------ | ------ |
| Fernando | [Repositorio](https://gitlab.com/fpgomez0311/fernando_markdown2) |
| Fernando | [Repositorio](https://gitlab.com/fpgomez0311/fernando_markdown2) |


_________________


**Práctica 3**

1. Creación de rama y añadiendo el archivo despliegue.md

![7](Imagenes/7.PNG)

2. Merge Directo

![8](Imagenes/8.PNG)

3. Merge con conflicto

![9](Imagenes/9.PNG)

4. Arreglo del conflicto (modificando el despliegue.md en la rama FERNANDO e igualandolo a la rama master)

![10](Imagenes/10.PNG)

5. Tag y borrar la rama

git tag v0.2 

![11](Imagenes/11.PNG)





